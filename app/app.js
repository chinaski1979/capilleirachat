import React from 'react';
import Chat from '../src/';
import qs from 'query-string';

const query = qs.parse(location.search);
const config = {
  socket : query.socket || 'http://190.241.12.163:11010/',
  room   : query.room || 'ComponentTest',
  mode   : query.mode || 'player',
  user   : query.user || 'Test Player'
};

class App extends React.Component {

  constructor (props) {
    super(props);
  }

  render () {
    return <div>
      <div><strong>Socket:</strong> {this.props.socket}</div>
      <div><strong>Room:</strong> {this.props.room}</div>
      <div><strong>Mode:</strong> {this.props.mode}</div>
      <div><strong>User:</strong> {this.props.user}</div>
      <hr />
      <Chat socket={this.props.socket} room={this.props.room} mode={this.props.mode} user={this.props.user} />
    </div>;
  }

}

React.render(<App socket={config.socket} room={config.room} mode={config.mode} user={config.user} />, document.body);
