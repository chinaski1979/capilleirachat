import React from 'react';

class ChatForm extends React.Component {

  constructor (props) {
    super(props);
    this.addMessage = this.addMessage.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  componentDidMount () {
    this.refs.newMessage.getDOMNode().focus();
  }

  addMessage () {
    let input = this.refs.newMessage.getDOMNode();
    this.props.onAddMessage(input.value);
    input.value = '';
    input.focus();
  }

  onKeyDown (e) {

    if (e.keyCode === 13) {
      this.addMessage();
    }

  }

  render () {
    return <div>
      <input
        placeholder="Your message..."
        ref="newMessage"
        onKeyDown={this.onKeyDown}
        autofocus="true" />
    </div>;
  }

}

export default ChatForm;
