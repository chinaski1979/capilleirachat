import React from 'react';

class ChatItem extends React.Component {

  constructor (props) {
    super(props);
  }

  render () {
    let item;
    const { message, player, me, info } = this.props;

    if (info) {
      item = <li><em>{message}</em></li>;
    }
    else if (me) {
      item = <li><strong>Me: {message}</strong></li>;
    }
    else {
      item = <li>{player}: {message}</li>;
    }

    return item;
  }

}

export default ChatItem;
