// Third parties.
import React from 'react';
import connectToStores from 'alt/utils/connectToStores';

// Actions & Stores.
import ChatActions from '../../actions/ChatActions';
import ChatStore from '../../stores/ChatStore';

// UI Components.
import ChatItem from '../ChatItem';
import ChatForm from '../ChatForm';


@connectToStores
class ChatView extends React.Component {

  static getStores () {
    return [ ChatStore ];
  }

  static getPropsFromStores () {
    return ChatStore.getState();
  }

  constructor (props) {
    super(props);
    this.addMessage = this.addMessage.bind(this);
    ChatActions.connect({ socketUrl : this.props.socket, mode : this.props.mode, room : this.props.room, user : this.props.user });
  }

  addMessage (text) {
    ChatActions.addMessage(text);
  }

  render () {
    let messages = this.props.chatMessages.map((message) => {
      return <ChatItem info={message.info} me={message.me} player={message.player} message={message.message} />;
    });

    let chatForm;

    if (this.props.mode === 'player') {
      chatForm = <ChatForm onAddMessage={this.addMessage} />;
    }

    return <div>
      <ul>{messages}</ul>
      {chatForm}
    </div>;
  }

}

export default ChatView;
