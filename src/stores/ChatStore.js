import alt from '../alt';
import ChatActions from '../actions/ChatActions';

@alt.createStore
class ChatStore {

  constructor () {
    this.state = { chatMessages : [] };
    this.bindActions(ChatActions);
  }

  onAddMessage (message) {
    message = { message : message, me : true };
    this.setState({ chatMessages : this.state.chatMessages.concat(message) });
  }

  onNewMessage (message) {
    this.setState({ chatMessages : this.state.chatMessages.concat(message) });
  }

  onNewInformationMessage (message) {
    message = { message : message, info : true };
    this.setState({ chatMessages : this.state.chatMessages.concat(message) });
  }

}

export default ChatStore;
